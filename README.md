# akalab

## Create sudo user

```
$ su root
# usermod -aG sudo ubuntu
# exit
$ newgrp sudo
```

## Basic software

```
$ sudo apt install byobu emacs-nox
```

## Prepare RAID

```
# Prepare one disk.
$ sudo fdisk /dev/sda
# Create a new empty GPT partition table.
g
# Add a new partition.
n
[accept all the default values]
# Change a partition type to Linux RAID.
t
29
# Write and exit.
w
# Apply the same configuration to all the other disks.
$ sudo sfdisk -d /dev/sda > /home/ubuntu/disk-layout.txt
$ for disk in `sudo fdisk -l | grep "^Disk \/dev\/sd[b-r]" | awk '{print $2}' | cut -d: -f1`;do sudo sfdisk  $disk < /home/ubuntu/disk-layout.txt;done;
# Create the raid array.
# mdadm --create --verbose --level=5 --metadata=1.2 --chunk=256 --raid-devices=15 /dev/md/raid /dev/sda1 /dev/sdb1 /dev/sdc1 /dev/sdd1 /dev/sde1 /dev/sdf1 /dev/sdg1 /dev/sdh1 /dev/sdj1 /dev/sdk1 /dev/sdl1 /dev/sdm1 /dev/sdn1 /dev/sdo1 /dev/sdp1 --spare-devices=2 /dev/sdq1 /dev/sdr1
```

## Servers

### 192.168.1.103

Ubuntu archive on IPFS

### 192.168.1.104

Ethereum archive node.

### 192.168.1.110

Polkadot validator.
